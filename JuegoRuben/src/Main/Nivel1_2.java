package Main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import Core.Field;
import Core.Sprite;
import Core.Window;

/**
 * 
 * @author Ruben Moreno Mora
 * @version 2
 *
 */
public class Nivel1_2 {

	static ArrayList<Sprite> sprites = new ArrayList<>();
	static ArrayList<Enemigo> Dinos = new ArrayList<Enemigo>();

	static Window w = WindowSingelton.getWindowSingleton(Main.f);
	
	static DinoNaranja Dino1 = new DinoNaranja("Dino1", 780, 540 , 840, 600, "IMG/enemigo.gif");

	static DinoVerde Dino2 = new DinoVerde("Dino2", 690, 940, 750,1000, "IMG/enemigo2.gif");
	
	static Pj MB;

	public static void main(String[] args) {

		MB = Pj.getPj("MeatBoy", 120, 590, 170, 640, "IMG/MeatBoyQuieto.png");
		Main.f.background = "IMG/fondoLvL2.jpg";
		
		Cuerda.rope(Main.f);
		
		Mapa.SegundoNivel(Main.f);

		while (true) {

			input();
			ArrayList<Sprite> sprites = new ArrayList<>();

			sprites.addAll(Mapa.nivel2);	
			
			Cuerda.subiendo();
			moveDinos();
				
			gravity();

			if (Main.f.getCurrentMouseX() != -1) {
				System.out.println(Main.f.getMouseX() + " " + Main.f.getMouseY());
			}
			if (w.getKeysDown().contains('l') || Nivel1_2.MB.collidesWithList(Mapa.puertaNivel2Menu).size() > 0 ) {
				cleanUP();
				sprites.clear();				
				Main.main(null);
				break;
			}

			Main.f.draw(sprites);

			try {
				Thread.sleep(35);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	/**
	 * cleanUP
	 * 
	 * por cada enemigo que se encuentre en la lista Dinos
	 * utiliza su funcion de moviminto y colision 
	 * 
	 */	
	private static void moveDinos() {
		for (Enemigo s : Dinos) {
			s.movimiento();
			s.colision();	
		}
		
	}

	
	/**
	 * cleanUP
	 * 
	 * su funcion es limpiar todas las listas usadas en el nivel
	 * 
	 * 
	 */	
	private static void cleanUP() {
		Dinos.clear();
		Mapa.sp2.clear();
		Mapa.nivel2.clear();
		Main.f.clear();
		Mapa.platLvL2.clear();
		Mapa.pinchosLvL2.clear();
		Mapa.bloquesIzq.clear();
		Mapa.bloquesDer.clear();
		Mapa.bloqueSalto.clear();		
	}

	/**
	 * Input
	 * 
	 * Detecta que tecla tienes presionada para moverte A= izquierda D = derecha W =
	 * saltar
	 * 
	 */
	private static void input() {
		if (w.getPressedKeys().contains('a')) {

			MB.move('a');
			MB.changeImage("IMG/PJtest.gif");
			if (MB.isOnColumn(Main.f)) {
				MB.getSided(Main.f);
			}

		}
		if (w.getPressedKeys().contains('d')) {

			MB.move('d');
			MB.changeImage("IMG/CaminarDerecha.gif");
			if (MB.isOnColumn(Main.f)) {
				MB.getSided(Main.f);
			}

		}
		if (w.getPressedKeys().contains('w')) {
			MB.jump();

		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

		if (w.getPressedKeys().contains('d') && w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

	}

	/**
	 * Gravity
	 * 
	 * le paso fall que es la funcion de saltar
	 * 
	 */

	private static void gravity() {
		MB.fall(Main.f);

	}

	
	
	

}