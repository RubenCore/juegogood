package Main;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Cuerda extends Sprite implements Escalable {

	public static ArrayList<Sprite> cuerdas = new ArrayList<>();
	static boolean GoingUp = false;

	public Cuerda(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
		terrain = false;
	}
	
	
	/**
	 * 
	 * rope
	 * 
	 * genera las escaleras y las a�ade a la lista
	 * 
	 */
	public static void rope(Field F) {

		Cuerda cuerda1 = new Cuerda("Cuerda1", 40, 45, 48, 580, "IMG/rope.png");
		Cuerda cuerda2 = new Cuerda("Cuerda2", 1850, 325, 1858, 980, "IMG/rope.png");

		cuerdas.add(cuerda2);
		cuerdas.add(cuerda1);

	}
	
	/**
	 * subiendo
	 * 
	 * coge todos los sprites de cuerda que colisionen con el personaje
	 * esos sprites cuerda ejecutan su codigo escalar
	 * 
	 */
	public static void subiendo() {
		for (Sprite sprite : Nivel1_2.MB.collidesWithList(Cuerda.cuerdas)) {
			((Cuerda) sprite).escalar();			
		}
		if(GoingUp && !(Nivel1_2.MB.collidesWithList(Cuerda.cuerdas).size() > 0)) {
			Nivel1_2.MB.status = 0;			
			GoingUp= false;					
		}
	}

	/**
	 * escalar
	 * 
	 * lo que hace es mirar si colisiones con la lista de cuerdas, entonces si presionas u cambia el estado del personaje para que este subiendo 
	 * y si pulsas j cambia al estado que esta cayendo 
	 * 
	 */
	@Override
	public void escalar() {

		if (Nivel1_2.MB.collidesWithList(Cuerda.cuerdas).size() > 0) {
			//A LA U SUBES
			if (Nivel1_2.w.getPressedKeys().contains('u')) {
				Nivel1_2.MB.status = 1;
				GoingUp = true;
			}
			
			//A LA J BAjas
			if (Nivel1_2.w.getPressedKeys().contains('j')) {
				Nivel1_2.MB.status = 2;
				GoingUp = false;
			}
		}

	}


	@Override
	public void escalarT() {
		
	}

}
