package Main;

import Core.Field;
import Core.Sprite;

public class DinoVerde extends Enemigo {

	static Field f = Main.f;

	boolean status = false, onGround = false;
	
	Sprite lastJumpBlock = null;
	
	public DinoVerde(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}
	
	/**
	 * DinoVerde
	 * 
	 * es una copia del dino construido principalmente
	 * en todo el juego se trabaja a partir de la copia
	 * 
	 */
	
	public DinoVerde(DinoVerde d) {
		super(d.name, d.x1, d.y1, d.x2, d.y2, d.path);
		this.moveSpeed = 6;
		this.getSided(f);
		this.trigger = true;
	}
	

	/**
	 * movimiento
	 * 
	 * define en que direcion ira el enemigo, en este caso el dino
	 * 
	 */	
	@Override
	public void movimiento() {
		if (status) {
			this.x1 -= moveSpeed;
			this.x2 -= moveSpeed;

		} else if (!status) {
			this.x1 += moveSpeed;
			this.x2 += moveSpeed;

		}
		
	}

	/**
	 * colision
	 * 
	 * define la colision con los muros que limitan le recorrido del dinosaurio
	 * cada vez que colisiona cambia la animacion
	 * 
	 * ademas detecta la colision con un punto en el cual tiene que saltar
	 * 
	 */	
	@Override
	public void colision() {
		for (Sprite d : this.collidesWithField(f)) {
			
			if (Mapa.bloquesDer.contains(d)) {				
				status = true;
				changeImage("IMG/enemigoizq.gif");
				continue;

			} else if (Mapa.bloquesIzq.contains(d)) {
				status = false;
				changeImage("IMG/enemigo2.gif");
				continue;

			}
			
			if(d instanceof Mapa && !d.name.equals("bloquedesalto")) {
				onGround = true;
				this.setVelocity(0, 0);
				this.y2 = d.y1-2;
				this.y1 = y2-62;
			}
			
			if (Mapa.bloqueSalto.contains(d)) {
				if(onGround && d != lastJumpBlock) {
					lastJumpBlock = d;
					this.setConstantForce(0, 0.2f);					
					this.setForce(0, -2d);
					onGround = false;
				}
			}
		}	
		if(!collidesWithField(f).contains(lastJumpBlock)) {
			lastJumpBlock = null;
		}
		
	}

} 
