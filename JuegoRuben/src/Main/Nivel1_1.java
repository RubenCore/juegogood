package Main;

import java.util.ArrayList;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import Core.Field;
import Core.Sprite;
import Core.Window;

/**
 * 
 * @author Ruben Moreno Mora
 * @version 2
 *
 */

public class Nivel1_1 {

	/**
	 * Variables estaticas
	 */


	static Window w = WindowSingelton.getWindowSingleton(Main.f);

	static Pj MB;
	static PinchosSube pinchso3 = new PinchosSube("PinchosSube", 990, 515, 1040, 540, "IMG/pinchos50x25.png");
	static PinchosSube pinchso5 = new PinchosSube("pinchos que suben", 1190, 515, 1240, 540, "IMG/pinchos50x25.png");
	static PinchosSube pinchso6 = new PinchosSube("pinchos que suben", 1390, 515, 1440, 540, "IMG/pinchos50x25.png");
	static PinchosSube pinchso7 = new PinchosSube("pinchos que suben", 1590, 515, 1640, 540, "IMG/pinchos50x25.png");
	static PinchosSube pinchso8 = new PinchosSube("pinchos que suben", 590, 515, 640, 540, "IMG/pinchos50x25.png");
	static PinchosSube pinchso9 = new PinchosSube("pinchos que suben", 390, 515, 440, 540, "IMG/pinchos50x25.png");
	static PinchosSube pinchso10 = new PinchosSube("pinchos que suben", 190, 515, 240, 540, "IMG/pinchos50x25.png");
	static PinchosSube pinchso4 = new PinchosSube("PinchosSube", 790, 515, 839, 540, "IMG/pinchos50x25.png");
	static ArrayList<PinchosSube> pinchos = new ArrayList<PinchosSube>();
	static Trampa bloqueT = new Trampa("bloqueTrampa", 950, 230, 1010, 290, "IMG/trampa.png");
	static ArrayList<Disparo> d = new ArrayList<Disparo>();

	static Timer timer = new Timer();

	public static void main(String[] args) {
		timer = new Timer();
		Main.f.background = "IMG/TestFondo3.png";
		MB = Pj.getPj("MeatBoy", 11, 951, 61, 1001, "IMG/MeatBoyQuieto.png");
		
		iniciarPinchos();
		bala();
		Escalera.escala(Main.f);
		Mapa.primerNivel(Main.f);

		while (true) {
			movimientoPinchos();
			input();
			ArrayList<Sprite> sprites = new ArrayList<>();

			sprites.addAll(d);
			sprites.addAll(Mapa.nivel1);

			Escalera.escalando();

			disparos();
			gravity();

			if (Main.f.getCurrentMouseX() != -1) {
				System.out.println(Main.f.getMouseX() + " " + Main.f.getMouseY());
			}
			if (MB.collidesWithList(Mapa.puertasNivel1).size() > 0 || w.getKeysDown().contains('l')) {
				
				cleanUP();
				timer.cancel();
				sprites.clear();
				Main.main(null);
				break;
			}

			Main.f.draw(sprites);

			try {
				Thread.sleep(35);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * cleanUP
	 * 
	 * su funcion es limpiar todas las listas usadas en el nivel
	 * 
	 * 
	 */
	private static void cleanUP() {
		pinchos.clear();
		d.clear();
		Mapa.sp.clear();
		Mapa.plataforma.clear();
		Mapa.pinchos.clear();
		Mapa.pinchosM.clear();
		Mapa.pinchosSube.clear();
		Mapa.trampas.clear();
		Mapa.puertasNivel1.clear();
		Mapa.nivel1.clear();
		Main.f.clear();
	}

	
	/**
	 * iniciarPinchos
	 * 
	 * inicia los pinchos en su posicion a partir de la copia y le pasa cuanto deve de subir y bajar
	 * 
	 * 
	 */	
	private static void iniciarPinchos() {
		pinchso5.statusInicial(26, 1190, 515);
		pinchso4.statusInicial(26, 790, 515);
		pinchso7.statusInicial(26, 1590, 515);
		pinchso9.statusInicial(26, 390, 515);
		pinchso3.statusInicial(false, 26, 990, 515);
		pinchso6.statusInicial(false, 26, 1390, 515);
		pinchso8.statusInicial(false, 26, 590, 515);
		pinchso10.statusInicial(false, 26, 190, 515);

		pinchos.add(pinchso5);
		pinchos.add(pinchso4);
		pinchos.add(pinchso7);
		pinchos.add(pinchso9);
		pinchos.add(pinchso3);
		pinchos.add(pinchso6);
		pinchos.add(pinchso8);
		pinchos.add(pinchso10);

	}

	/**
	 * Input
	 * 
	 * Detecta que tecla tienes presionada para moverte A= izquierda D = derecha W =
	 * saltar
	 * 
	 */
	private static void input() {
		if (w.getPressedKeys().contains('a')) {

			MB.move('a');
			MB.changeImage("IMG/PJtest.gif");
			if (MB.isOnColumn(Main.f)) {
				MB.getSided(Main.f);
			}

		}
		if (w.getPressedKeys().contains('d')) {

			MB.move('d');
			MB.changeImage("IMG/CaminarDerecha.gif");
			if (MB.isOnColumn(Main.f)) {
				MB.getSided(Main.f);
			}

		}
		if (w.getPressedKeys().contains('w')) {
			MB.jump();

		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

		if (w.getPressedKeys().contains('d') && w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

	}

	/**
	 * Gravity
	 * 
	 * le paso fall que es la funcion de saltar
	 * 
	 */

	private static void gravity() {
		MB.fall(Main.f);

	}

	/**
	 * movimientoPinchos
	 * 
	 * por cada pincho en la lista hace la funcion subidaPinchos
	 * 
	 */	
	private static void movimientoPinchos() {
		for (PinchosSube p : pinchos) {
			p.subidaPinchos();
		}

	}

	/**
	 * Disparos
	 * 
	 * recorre la lista de los disparos, cuando colisona con un muro ese disparo se
	 * borra
	 * 
	 */

	private static void disparos() {
		for (int i = 0; i < d.size(); i++) {
			if (d.get(i).collision()) {
				d.remove(i);

				i--;
			}
		}

	}

	/**
	 * Bala
	 * 
	 * Cada 2.25 segundos a�ade una bala a la lista de disparos
	 * 
	 * @param b
	 */

	private static void bala() {

		TimerTask balaa = new TimerTask() {
			public void run() {
				d.add(MB.shoot());
				d.add(MB.shoot2());

			}
		};
		timer.scheduleAtFixedRate(balaa, 0, 2275);
	}
}
