package Main;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import Core.Field;
import Core.Sprite;
import Core.Window;


/**
 * 
 * @author Ruben Moreno Mora
 * @version 2
 *
 */

public class Main {
	/**
	 * Variables estaticas
	 */

	static ArrayList<Sprite> sprites = new ArrayList<>();
	static Field f = new Field();

	static Window w = WindowSingelton.getWindowSingleton(f);

	static Pj MB;
	static Texto t = new Texto("texto", 845, 880, 845, 890, "puerta 1");
	static Texto t2 = new Texto("texto", 835, 475, 835, 485, "puerta 2");
	static Texto tTienda = new Texto("texto", 1310, 615, 1310, 625, "tienda");
	static Texto controles = new Texto("texto", 210, 125, 210, 135, "controles");

	public static void main(String[] args) {
	
		t.textColor = 0xFFFFFF;
		t2.textColor = 0x000000;
		tTienda.textColor= 0x000000;
		controles.textColor= 0x66ff66;
		
		MB = Pj.getPj("MeatBoy", 120, 590, 170, 640, "IMG/MeatBoyQuieto.png");

		f.background = "IMG/fondopuertas.png";
		Escalera.escala(f);
		Mapa.BaseNivel(f);
		while (true) {
			
			String str1 ="Puerta Nivel 1";
			String str2 = "Puerta Nivel 2";
			String strTienda = "Tienda";
			String control="a = izquierda | d = derecha | w = saltar | u = subir escaleras/cuerdas | j = bajar escaleras/cuerdas | Atajos= t->Tienda, p ->Nivel 1, o -> Nivel2, l->Menu de Niveles";
			t.path = str1; 
			t2.path = str2;	
			tTienda.path = strTienda;
			controles.path= control;
			
			input();
			sprites.addAll(Mapa.nivel0);
			sprites.add(t);
			sprites.add(t2);
			sprites.add(tTienda);
			sprites.add(controles);
			gravity();

			if (f.getCurrentMouseX() != -1) {
				System.out.println(f.getMouseX() + " " + f.getMouseY());
			}
			if (MB.collidesWithList(Mapa.puertaNivel0).size() > 0 || w.getKeysDown().contains('p')) {
				cleanUP();
				sprites.clear();
				Nivel1_1.main(null);
				break;
			}
			if (MB.collidesWithList(Mapa.puertaNivel2).size() > 0 || w.getPressedKeys().contains('o')) {
				cleanUP();
				sprites.clear();
				Nivel1_2.main(null);
				break;
			}
			if ((MB.collidesWithList(Mapa.puertaNivelMenuTienda).size() > 0 && MB.puedesPasar != false)
					|| w.getPressedKeys().contains('t')) {
				cleanUP();
				sprites.clear();
				Tienda.main(null);
				break;
			}

			f.draw(sprites);

			try {
				Thread.sleep(35);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
		}
		
		

	}

	/**
	 * cleanUP
	 * 
	 * su funcion es limpiar todas las listas usadas en el nivel
	 * 
	 * 
	 */

	private static void cleanUP() {
		Mapa.sp0.clear();
		Mapa.puertaNivel0.clear();
		Mapa.puertaNivel2.clear();
		Mapa.nivel0.clear();
		f.clear();
	}

	/**
	 * Input
	 * 
	 * Detecta que tecla tienes presionada para moverte A= izquierda D = derecha W =
	 * saltar
	 * 
	 */

	private static void input() {
		if (w.getPressedKeys().contains('a')) {
			MB.move('a');
			MB.changeImage("IMG/PJtest.gif");
			if (MB.isOnColumn(f)) {
				MB.getSided(f);
			}

		}
		if (w.getPressedKeys().contains('d')) {
			MB.move('d');
			MB.changeImage("IMG/CaminarDerecha.gif");
			if (MB.isOnColumn(f)) {
				MB.getSided(f);
			}

		}
		if (w.getPressedKeys().contains('w')) {
			MB.jump();

		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

		if (w.getPressedKeys().contains('d') && w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

	}

	/**
	 * Gravity
	 * 
	 * le paso fall que es la funcion de saltar
	 * 
	 */

	private static void gravity() {
		MB.fall(f);

	}
}
