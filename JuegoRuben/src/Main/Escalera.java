package Main;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Escalera extends Sprite implements Escalable {
	public static ArrayList<Sprite> escaleras = new ArrayList<>();
	public static ArrayList<Sprite> escaleras2 = new ArrayList<>();

	static boolean subiendo = false;

	public Escalera(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
		
	}

	/**
	 * 
	 * Escala
	 * 
	 * genera las escaleras y las a�ade a la lista
	 * 
	 */

	public static void escala(Field f) {
		Escalera escalera1 = new Escalera("Escalera1", 1840, 550, 1890, 750, "IMG/escalera.png");

		Escalera escalera2 = new Escalera("escalera2", 10, 300, 55, 525, "IMG/escalera.png");

		Escalera escaleraT1 = new Escalera("escaleraT1", 520, 601, 565, 935, "IMG/escalera.png");
		
		Escalera escaleraT2 = new Escalera("escaleraT2", 520+855, 601, 565+855, 935, "IMG/escalera.png");
		
		Escalera escaleraT3 = new Escalera("escaleraT3",900, 320, 950, 930, "IMG/escalera.png");

		escaleras.add(escalera2);

		escaleras.add(escalera1);

		escaleras2.add(escaleraT1);
		
		escaleras2.add(escaleraT2);
		
		escaleras2.add(escaleraT3);
		

	}

	/**
	 * escalando
	 * 
	 * coge todos los sprites de escalera que colisionen con el personaje esos
	 * sprites escalera ejecutan su codigo escalar
	 * 
	 */

	public static void escalando() {
		for (Sprite sprite : Nivel1_1.MB.collidesWithList(Escalera.escaleras)) {
			((Escalera) sprite).escalar();
		}
		if (subiendo && !(Nivel1_1.MB.collidesWithList(Escalera.escaleras).size() > 0)) {
			Nivel1_1.MB.status = 0;
			Nivel1_1.MB.jump();
			subiendo = false;
		}

	}

	public static void escalandoT() {
		for (Sprite sprite : Tienda.MB.collidesWithList(Escalera.escaleras2)) {
			((Escalera) sprite).escalarT();
		}
		if (subiendo && !(Tienda.MB.collidesWithList(Escalera.escaleras2).size() > 0)) {
			Tienda.MB.status = 0;
			Tienda.MB.jump();
			subiendo = false;
		}
	}

	/**
	 * escalar
	 * 
	 * lo que hace es mirar si colisiones con la lista de escleras, entonces si
	 * presionas u cambia el estado del personaje para que este subiendo y si pulsas
	 * j cambia al estado que esta cayendo
	 * 
	 */
	@Override
	public void escalar() {
	
			if (Nivel1_1.MB.collidesWithList(Escalera.escaleras).size() > 0) {
				// A LA U SUBES
				if (Nivel1_1.w.getPressedKeys().contains('u')) {
					Nivel1_1.MB.status = 1;
					subiendo = true;
				}
				// A LA J BAjas
				if (Nivel1_1.w.getPressedKeys().contains('j')) {
					Nivel1_1.MB.status = 2;
					subiendo = false;
				}
			}	 

		}

	@Override
	public void escalarT() {
		if (Tienda.MB.collidesWithList(Escalera.escaleras2).size() > 0) {
			// A LA U SUBES
			if (Tienda.w.getPressedKeys().contains('u')) {
				Tienda.MB.status = 1;
				subiendo = true;
			}
			// A LA J BAjas
			if (Tienda.w.getPressedKeys().contains('j')) {
				Tienda.MB.status = 2;
				subiendo = false;
			}
		}
	}
	}

