package Main;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;

public class DinoNaranja extends Enemigo {

	static Field f = Main.f;

	boolean status;

	// true = derecha || false = izquierda

	public DinoNaranja(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		terrain=true;
		getSided(f);

	}
	
	/**
	 * DinoNaranja
	 * 
	 * es una copia del dino construido principalmente
	 * en todo el juego se trabaja a partir de la copia
	 * 
	 */
	public DinoNaranja(DinoNaranja d) {
		super(d.name, d.x1, d.y1, d.x2, d.y2, d.path);
		trigger = true;
		getSided(f);

	}
	
	/**
	 * movimiento
	 * 
	 * define en que direcion ira el enemigo, en este caso el dino
	 * 
	 */
	@Override
	public void movimiento() {
		if (status) {
			this.x1 -= moveSpeed;
			this.x2 -= moveSpeed;

		} else if (!status) {
			this.x1 += moveSpeed;
			this.x2 += moveSpeed;

		}

	}
	
	/**
	 * colision
	 * 
	 * define la colision con los muros que limitan le recorrido del dinosaurio
	 * cada vez que colisiona cambia la animacion
	 * 
	 */

	@Override
	public void colision() {
		for (Sprite d : this.collidesWithField(f)) {
			if (Mapa.bloquesDer.contains(d)) {				
				status = true;
				changeImage("IMG/enemigoizq.gif");

			} else if (Mapa.bloquesIzq.contains(d)) {
				status = false;
				changeImage("IMG/enemigo.gif");

			}
		}

	}

}
