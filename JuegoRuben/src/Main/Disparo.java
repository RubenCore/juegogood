package Main;

import Core.Sprite;

public class Disparo extends Sprite {

	public Disparo(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.physicBody = true;
		this.trigger= true;

	}

	/**
	 * Move
	 * 
	 * movimiento de la vala 
	 * 
	 */
	
	public void move() {
		this.setVelocity(4, 0);
	}
	
	/**
	 * Move
	 * 
	 * movimiento de la vala 
	 * 
	 */
	
	public void move2() {
		this.setVelocity(-4, 0);
	}

	/**
	 * 
	 * Collision
	 * 
	 * Mira si la bala colisiona con el PJ o un muro
	 * si colisiona con el PJ muere y aparece en el punto de inicio
	 * si la bala colisona con el muro  se borra 
	 * 
	 */
	
	public boolean collision() {

		if (this.collidesWith(Nivel1_1.MB)) {
			Nivel1_1.MB.x1 = 11;
			Nivel1_1.MB.y1 = 951;
			Nivel1_1.MB.x2 = 61;
			Nivel1_1.MB.y2 = 1001;
		}
		if (this.collidesWithList(Mapa.plataforma).size() > 0) {
			
			this.delete();
			return true;
		}else {
			return false;
		}

	}

}
