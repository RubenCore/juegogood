package Main;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Pj extends Sprite {

	static Field f = Main.f;

	private static Pj MB = null;

	private Pj(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);

	}

	/**
	 * getPj
	 * 
	 * copia el personaje si este es null, si no coge los parametros del personaje
	 * creado y devuelve el mismo
	 * 
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param path
	 * @return MB - devuelve la instancia del singleton
	 */
	public static Pj getPj(String name, int x1, int y1, int x2, int y2, String path) {
		if (MB == null) {
			MB = new Pj(name, x1, y1, x2, y2, path);
		} else {
			MB.name = name;
			MB.x1 = x1;
			MB.x2 = x2;
			MB.y1 = y1;
			MB.y2 = y2;
			MB.path = path;
		}
		return MB;
	}

	int status; // 0 en terra, 1 pujant 2 caient
	int jumpdistance;
	// int vida = 4;
	
	boolean puedesPasar = false;
	int velx1 = 8;
	int velx2 = 8;
	int mejoraSalto = 0;
	/**
	 * Move
	 * 
	 * controla la velocidad del movimiento y mira si colisiona con los muros,
	 * pinchos, etc
	 * 
	 * 
	 */
	public void move(char c) {
		checkCollisionGeneral();
		// TODO Auto-generated method stub
		if (c == 'd') {
			x1 += velx1;
			x2 += velx2;
			GoBack();

			checkCollisionDinoLaterales();

		} else if (c == 'a') {
			x1 -= velx1;
			x2 -= velx2;
			GoBack();

			checkCollisionDinoLaterales();
		}
	}

	/**
	 * Fall
	 * 
	 * Es una especie de gravedad le permite saltar x distancia y bajar la misma
	 *
	 */
	// 0 en terra, 1 pujant 2 caient
	public void fall(Field f) {
		if (status == 1) {
			y1 -= 10;
			y2 -= 10;
			jumpdistance--;
			GoBack();

			if (jumpdistance == 0) {
				status = 2;
			}
			if (isOnCeiling(f)) {
				getCeiling(f);

			}
			// GoBack();
		}

		else if (isGrounded(f)) {
			status = 0;
			getGrounded(f);

		} else {
			status = 2;
			y1 += 10;
			y2 += 10;
			checkCollisionsOnFall();

			GoBack();
		}

	}

	/**
	 * checkCollisionDino
	 * 
	 * coje todos los sprites con los que esta colisionando el pj, si alguno de esos
	 * pertenecen a la lista de dinosaurios lo destruye
	 * 
	 */
	private void checkCollisionsOnFall() {
		for (Sprite d : this.collidesWithField(f)) {
			if (Nivel1_2.Dinos.contains(d)) {
				d.delete();
			}
		}

	}

	private void checkCollisionGeneral() {
		for (Sprite d : this.collidesWithField(f)) {
			if (Items.getInstance(f).itemList.contains(d) || Items.getInstance(f).coinList.contains(d)) {
				Item item = (Item) d;
				if (item.type == ItemType.SPEED) {
					this.velx1 += 4;
					this.velx2 += 4;
				} else if (item.type == ItemType.JUMP) {
					this.mejoraSalto=7;
				}else if (item.type == ItemType.COIN) {
					this.puedesPasar=true;
				}
				d.delete();
			}
		}
	}

	/**
	 * checkCollisionDino
	 * 
	 * coje todos los sprites con los que esta colisionando el pj, si el pj
	 * colisiona lateralmente este es devuelto a su posicion de respawn
	 * 
	 */
	private void checkCollisionDinoLaterales() {
		for (Sprite d : this.collidesWithField(f)) {
			if (Nivel1_2.Dinos.contains(d)) {
				x1 = 1845;
				y1 = 60;
				x2 = 1895;
				y2 = 110;
			}
		}
	}

	/**
	 * Jump
	 * 
	 * delimita la distancia de salto
	 * 
	 */

	public void jump() {
		// TODO Auto-generated method stub

		if (status == 0) {
			status = 1;
			jumpdistance = 10 + mejoraSalto;

		}

	}

	/**
	 * 
	 * GoBack
	 * 
	 * Mira si colisiona con los pinchos, si es as� el PJ vuelve a su posicion
	 * inicial
	 * 
	 */

	public void GoBack() {
		if (collidesWithList(Mapa.pinchos).size() > 0 || collidesWithList(Mapa.pinchosSube).size() > 0) {

			x1 = 11;
			y1 = 951;
			x2 = 61;
			y2 = 1001;

		}

		if (collidesWithList(Mapa.pinchosLvL2).size() > 0) {
			x1 = 1845;
			y1 = 60;
			x2 = 1895;
			y2 = 110;
		}
	}

	/**
	 * Shoot
	 * 
	 * Creo la bala con el tama�o y la imagen deseada y el paso la funcion de
	 * moverse
	 * 
	 */

	public Disparo shoot() {
		Disparo d = new Disparo("bala", Nivel1_1.bloqueT.x1 + 6, Nivel1_1.bloqueT.y1 + 10, Nivel1_1.bloqueT.x2 + 5,
				Nivel1_1.bloqueT.y2 - 20, "IMG/cosaQueMata.png");
		d.move();

		return d;

	}

	/**
	 * Shoot2
	 * 
	 * Creo la bala con el tama�o y la imagen deseada y el paso la funcion de
	 * moverse
	 * 
	 */
	public Disparo shoot2() {
		Disparo d2 = new Disparo("bala", Nivel1_1.bloqueT.x1 - 5, Nivel1_1.bloqueT.y1 + 10, Nivel1_1.bloqueT.x2 - 6,
				Nivel1_1.bloqueT.y2 - 20, "IMG/cosaQueMata2.png");
		d2.move2();
		return d2;

	}

}
